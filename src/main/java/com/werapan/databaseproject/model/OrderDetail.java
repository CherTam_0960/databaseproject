/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.werapan.databaseproject.model;

/**
 *
 * @author kitti
 */
public class OrderDetail {

    private int id;
    Product product;
    private String ProductName;
    private double productPrice;
    private int qty;
    private Orders order;

    public OrderDetail(int id, Product product, String ProductName, double productPrice, int qty, Orders order) {
        this.id = id;
        this.product = product;
        this.ProductName = ProductName;
        this.productPrice = productPrice;
        this.qty = qty;
        this.order = order;
    }

    public OrderDetail(Product product, String ProductName, double productPrice, int qty, Orders order) {
        this.product = product;
        this.ProductName = ProductName;
        this.productPrice = productPrice;
        this.qty = qty;
        this.order = order;
    }

    public OrderDetail() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String ProductName) {
        this.ProductName = ProductName;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public Orders getOrder() {
        return order;
    }

    public void setOrder(Orders order) {
        this.order = order;
    }

    public double getTotal() {
        return qty * productPrice;
    }

    @Override
    public String toString() {
        return "OrderDetail{" + "id=" + id + ", product=" + product + ", ProductName=" + ProductName + ", productPrice=" + productPrice + ", qty=" + qty + "}";
    }

}
