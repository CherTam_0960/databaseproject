/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.service;

import com.werapan.databaseproject.dao.UserDao;
import com.werapan.databaseproject.model.Users;
import java.util.List;

/**
 *
 * @author werapan
 */
public class UserService {

    public Users login(String login, String password) {
        UserDao userDao = new UserDao();
        Users user = userDao.getByLogin(login);
        if (user != null && user.getPassword().equals(password)) {
            return user;
        }
        return null;
    }

    public List<Users> getUsers() {
        UserDao userDao = new UserDao();
        return userDao.getAll(" user_login asc");

    }

    public Users addNew(Users editUser) {
        UserDao userDao = new UserDao();
        return userDao.save(editUser);
    }

    public Users update(Users editUser) {
        UserDao userDao = new UserDao();
        return userDao.update(editUser);
    }

    public int delete(Users editUser) {
        UserDao userDao = new UserDao();
        return userDao.delete(editUser);
    }

}
